var express = require('express');
const { rawListeners } = require('../app');
var router = express.Router();

/* GET food types listing. */
router.get('/', function(req, res, next) {
    res.send('all food json list');
});

/* GET specific food type information by name. */
router.get('/:foodType', function(req, res, next) {
    res.send(`specific food information: ${req.params.foodType}!`);
});

module.exports = router;