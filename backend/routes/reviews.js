var express = require('express');
var router = express.Router();
var queries = require('../queries/reviews');

/* GET reviews listing. */
router.get('/', function(req, res, next) {
  res.send('send json with all reviews information');
});

/* GET reviews by food type. */
/*router.get('/:foodType', function(req, res, next) {
  res.send(`sending all information for food type: ${req.params.foodType}`);
});*/

/* GET reviews by user.*/
router.get('/byuser/:userId(\\d+)', function(req, res, next) {
    res.send(`sending all information for food type: ${req.params.userId}`);
});

/* GET reviews by kitchen id.*/
router.get('/bykitchen/:kitchenId(\\d+)', function(req, res, next) {
  const id = parseInt(req.params.kitchenId)
  queries.getReviewByKitchenId(req, res, id);
  //res.send(`sending all information for food type: ${req.params.userId}`);
});


/* GET revie
router.post('/kitchenAverage', function(req, res, next) {
  queries.kitchenAverage(res, req.body.startDate, req.body.endDate, req.body.selectedKitchen, req.body.selectedMeal);
  //res.send(`sending all information for food type: ${req.params.userId}`);
});*/

/* POST add new review*/
router.post('/create', function(req, res, next) {
    res.send(`add new record to db: ${req.body}`);
});



module.exports = router;
