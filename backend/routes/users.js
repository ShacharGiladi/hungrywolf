var express = require('express');
var router = express.Router();
var queries = require('../queries/users');

/* GET users listing. */
router.get('/', function(req, res, next) {
  queries.getUsers(req, res);
  //res.send('send json with all users information');
});

/* GET user information by id. */
router.get('/:userId(\\d+)', function(req, res, next) {
  const id = parseInt(req.params.userId)
  queries.getUserById(req, res, id);
  //res.send(`sending all information for user id: ${id}`);
});

/* GET user information by name. */
router.get('/:userFirstName-:userLastName', function(req, res, next) {
  res.send(`sending all information for user: ${req.params.userFirstName} ${req.params.userLastName}.`);
});

module.exports = router;
