var express = require('express');
const { rawListeners } = require('../app');
var router = express.Router();
var queries = require('../queries/kitchens');

/* GET kitchens list. */
router.get('/', function(req, res, next) {
    queries.getKitchens(req, res);
    //res.send("These kitchens!")
});

/* GET kitchen information by id. */
router.get('/:kitchenId(\\d+)', function(req, res, next) {
    const id = parseInt(req.params.kitchenId)
    queries.getKitchenById(req, res, id);
    //res.send(`sending all information for user id: ${id}`);
  });
  
module.exports = router;