var express = require('express');
var main_db = require('../main_db')();

const getKitchens = (request, response) => {
  main_db.connect((err, client, release) => {
    if (err) {
      throw error
    }
    client.query('SELECT * FROM public.kitchens ORDER BY id ASC', (error, results) => {
      release()
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  })
}

const getKitchenById = (request, response, id) => {
  main_db.connect((err, client, release) => {
    if (err) {
      throw error
    }
    client.query('SELECT * FROM public.kitchens WHERE id = $1', [id] , (error, results) => {
      release()
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  })
}
  
module.exports.getKitchens = getKitchens
module.exports.getKitchenById = getKitchenById