var express = require('express');
var main_db = require('../main_db')();

/*const kitchenAverage = (response, date_start, date_end, kitchenId, meal) => {
  main_db.connect((err, client, release) => {
    if (err) {
      throw error
    }
    client.query('SELECT * FROM public.reviews ' + 
                    'WHERE (filled_at BETWEEN \#$1\# AND \#$2\#) AND' + 
                    ' (kitchen_id = $3) AND (meal = $4)', [date_start, date_end, kitchenId, meal], (error, results) => {
      release()
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  })
}*/

const getReviewByKitchenId = (request, response, id) => {
    main_db.connect((err, client, release) => {
      if (err) {
        throw error
      }
      client.query('SELECT * FROM public.reviews WHERE kitchen_id = $1', [id] , (error, results) => {
        release()
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
      })
    })
  }
  
//module.exports.kitchenAverage = kitchenAverage
module.exports.getReviewByKitchenId = getReviewByKitchenId