var express = require('express');
var main_db = require('../main_db')();

const getUsers = (request, response) => {
  main_db.connect((err, client, release) => {
    if (err) {
      throw error
    }
    client.query('SELECT * FROM public.users ORDER BY id ASC', (error, results) => {
      release()
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  })
}
  
const getUserById = (request, response, id) => {
  main_db.connect((err, client, release) => {
    if (err) {
      throw error
    }
    client.query('SELECT * FROM public.users WHERE id = $1', [id] , (error, results) => {
      release()
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  })
}

module.exports.getUsers = getUsers
module.exports.getUserById = getUserById